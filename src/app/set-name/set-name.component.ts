import { Component, OnInit } from '@angular/core';
import { UnService } from '../state/un.service';

@Component({
  selector: 'app-set-name',
  templateUrl: './set-name.component.html',
  styleUrls: ['./set-name.component.sass']
})
export class SetNameComponent implements OnInit {

  constructor(
    private nameService: UnService
  ) { }

  ngOnInit() {
  }

  setName(user: string) {
    console.log("actualizado nombre de usuario en el store -> ", user);
    this.nameService.setName(user);
  }

  resetName() {
    console.log("actualizado nombre de usuario en el store -> '' ");
    this.nameService.resetName();
  }

}