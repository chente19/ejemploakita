import { Component, OnInit } from '@angular/core';
// import { Observable } from 'rxjs';
import { NameQuery } from '../state/laQuery';

@Component({
  selector: 'app-get-name',
  templateUrl: './get-name.component.html',
  styleUrls: ['./get-name.component.sass'],
})


export class GetNameComponent implements OnInit {

  // name$: Observable<string>;
  salida: any;

  constructor(
    // aqui el servicio que hace consultas al store, privado please
    private nameQuery: NameQuery
  ) { }

  ngOnInit()  {
    //this.name$ = this.nameQuery.getName$;
    this.salida = this.nameQuery.getName$;
  }
}